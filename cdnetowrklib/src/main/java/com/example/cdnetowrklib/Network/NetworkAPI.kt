package com.example.networkretro.Network

import com.example.networkretro.ApiClient.UserClient
import com.example.networkretro.Interface.ApiInterface
import com.example.networkretro.Utils.Constant
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object NetworkAPI {


    fun getUserDetails(serviceUrl: String): String {
        var serviceResponse: String = ""
        var apiServices = UserClient.getClient(serviceUrl).create(ApiInterface::class.java)
        apiServices.userDetails().enqueue(object : Callback<ResponseBody> {
            override fun onFailure(response: Call<ResponseBody>, t: Throwable) {
                serviceResponse = t.toString()
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                serviceResponse = response.body().toString()
            }

        })
        return serviceResponse
    }
}