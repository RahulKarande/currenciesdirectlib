package com.example.cd_businesswrapperparserlib.wrapper

import android.text.TextUtils
import com.example.cd_businesswrapperparserlib.UtilsParserConverter.ResponseParserConverter
import com.example.cd_businesswrapperparserlib.`interface`.OnGetParserReponseListener
import com.example.cd_businesswrapperparserlib.`interface`.OnGetResponse
import com.example.networkretro.Network.NetworkAPI

object BusinessWrapperParser : OnGetResponse {

    fun getUserDetails(responseListener: OnGetParserReponseListener, baseUrl: String) {
        this.onGetResponse(NetworkAPI.getUserDetails(baseUrl), responseListener)
    }

    override fun onGetResponse(response: String, responseListener: OnGetParserReponseListener) {
        if (TextUtils.isEmpty(response)) {
            responseListener.failure(response)
        } else {
            responseListener.onSucess(response)
        }

    }


}