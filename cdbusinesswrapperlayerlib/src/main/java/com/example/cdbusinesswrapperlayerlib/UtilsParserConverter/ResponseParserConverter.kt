package com.example.cd_businesswrapperparserlib.UtilsParserConverter


import com.google.gson.Gson

object ResponseParserConverter {

    fun getConvertReponseToObject(respone: String, parserObj: Any): Any {
        val gson: Gson = Gson()

        return gson.fromJson(respone, parserObj::class.java)
    }
}